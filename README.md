# GreyFox

A single page website created for Grey Fox graphic studio. Technologies used: HTML, SCSS and Bootstrap version 4.1.3.<br>
In the future, I will probably add client graphics and texts and publish the project.<br>
Live: [_https://grey-fox-just-gru.netlify.app_](https://grey-fox-just-gru.netlify.app).
